import csv
import sys
from datetime import datetime
import time
import calendar
from datetime import date
import holidays

fmt1 = '%Y%m%d_%H%M%S.jpg'
fmt2 = '%m/%d/%Y %H:%M'

def processdata(data):
	newdata = []

	date_obj = datetime.strptime(str(data[3]), fmt2)
	# date
	if date_obj.weekday() > 3: weekend = 1
	else: weekend = 0
	# holiday
	if date_obj in holidays.UnitedStates(): holiday = 1
	else: holiday = 0

	newdata.extend([weekend, holiday])

	# surface temperature in fahrenheit
	temp = data[5]
	newdata.append(temp)

	# dew point in fahrenheit, measure of atmospheric moisture
	dewpoint = data[6]
	newdata.append(dewpoint)

	cloudcover = data[10]
	newdata.append(cloudcover)

	# precipitation in the previous hour in inches
	precip = data[15]
	newdata.append(precip)

	snowfall = data[20]
	newdata.append(precip)

	windspeed = data[13]
	newdata.append(windspeed)

	return newdata

# first one is the text file with the relevant file names
# second one is the file with the unprocessed weather data
# third one is the processed weather data
with open(sys.argv[1], 'r') as all_images, open(sys.argv[2], 'r') as f1, open(sys.argv[3], 'w') as f2:
	reader = csv.reader(f1)
	writer = csv.writer(f2)

	next(reader) # get past labels

	# write out the labels we've got
	writer.writerow(["weekend", "holiday", "temp", "dewpoint", "cloudcover", "precip", "snow", "windspeed"])

	# PROCESS FIRST IMAGE
	image = next(all_images).rstrip('\n')
	time0 = calendar.timegm(time.strptime(image, fmt1))

	# FIND THE RIGHT DATE TO START ON
	data1 = next(reader)
	time1 = calendar.timegm(time.strptime(str(data1[3]), fmt2))
	data2 = data1
	time2 = time1
	while (time2 < time0):
		data1 = data2
		time1 = time2
		data2 = next(reader)
		time2 = calendar.timegm(time.strptime(str(data1[3]), fmt2))

	# choose the time that is closer
	if (time1 - time0) < (time2 - time0):
		writer.writerow([image] + processdata(data1))
	else:
		writer.writerow([image] + processdata(data2))

	# PROCESS OTHER IMAGES
	for image in all_images:
		image = image.rstrip('\n')
		time0 = calendar.timegm(time.strptime(image, fmt1))
		while (time0 > time2):
			temp = next(reader)
			temptime = calendar.timegm(time.strptime(temp[3], fmt2))
			data1 = data2
			time1 = time2
			data2 = temp
			time2 = temptime

		# choose the time that is closer
		if (time1 - time0) < (time2 - time0):
			writer.writerow([image] + processdata(data1))
		else:
			writer.writerow([image] + processdata(data2))
