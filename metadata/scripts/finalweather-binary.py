
# python finalweather-binary.py 21656.txt 2251857142_5-1-2006_7-19-2016_hourly.csv weather-binary-21656.csv

import csv
import sys
from datetime import datetime
from datetime import date
import time
import calendar
import holidays

fmt1 = '%Y%m%d_%H%M%S.jpg'
fmt2 = '%m/%d/%Y %H:%M'

def processdata(data):
	newdata = []

	date_obj = datetime.strptime(str(data[3]), fmt2)
	# date
	if date_obj.weekday() > 3: weekend = 1
	else: weekend = 0
	# holiday
	if date_obj in holidays.UnitedStates(): holiday = 1
	else: holiday = 0

	newdata.extend([weekend, holiday])

	# surface temperature in fahrenheit
	temp = float(data[5])
	if temp > 85:
		hot = 1
	else: hot = 0
	if temp < 32:
		freezing = 1
	else: freezing = 0
	newdata.extend([hot, freezing])

	# dew point in fahrenheit, measure of atmospheric moisture
	dewpoint = float(data[6])
	if dewpoint > 64:
		humid = 1
	else: humid = 0
	if dewpoint < 50:
		dry = 1
	else: dry = 0
	newdata.extend([humid, dry])

	cloudcover = float(data[10])
	if cloudcover < 33:
		c1 = 1
	else: c1 = 0
	if cloudcover > 66:
		c2 = 1
	else: c2 = 0
	newdata.extend([c1, c2])

	# precipitation in the previous hour in inches
	precip = float(data[15])
	if precip > 0:
		binaryPrecip = 1
	else: binaryPrecip = 0
	if precip > 0 and precip < 0.39:
		lightmoderate = 1
	else: lightmoderate = 0
	if precip > 0.3:
		heavy = 1
	else: heavy = 0
	newdata.extend([binaryPrecip, lightmoderate, heavy])

	snowfall = float(data[20])
	if snowfall > 0:
		snowBinary = 1
	else: snowBinary = 0
	newdata.extend([snowBinary])

	windspeed = float(data[13])
	if windspeed > 39:
		gale = 1
	else: gale = 0
	if windspeed > 54:
		storm = 1
	else: storm = 0
	newdata.extend([gale, storm])

	return newdata

# first one is the text file with the relevant file names
# second one is the file with the unprocessed weather data
# third one is the processed weather data
with open(sys.argv[1], 'r') as all_images, open(sys.argv[2], 'r') as f1, open(sys.argv[3], 'w') as f2:
	reader = csv.reader(f1)
	writer = csv.writer(f2)

	next(reader) # get past labels

	# write out the labels we've got
	writer.writerow(["weekend", "holiday", "hot", "freezing", "humid", "dry", "not cloudy", "pretty cloudy", "precip", "lightmoderate rain", "heavy rain", "snow", "gale", "storm"])

	# PROCESS FIRST IMAGE
	image = next(all_images).rstrip('\n')
	time0 = calendar.timegm(time.strptime(image, fmt1))

	# FIND THE RIGHT DATE TO START ON
	data1 = next(reader)
	time1 = calendar.timegm(time.strptime(str(data1[3]), fmt2))
	data2 = data1
	time2 = time1
	while (time2 < time0):
		data1 = data2
		time1 = time2
		data2 = next(reader)
		time2 = calendar.timegm(time.strptime(str(data1[3]), fmt2))

	# choose the time that is closer
	if (time1 - time0) < (time2 - time0):
		writer.writerow([image] + processdata(data1))
	else:
		writer.writerow([image] + processdata(data2))

	# PROCESS OTHER IMAGES
	for image in all_images:
		image = image.rstrip('\n')
		time0 = calendar.timegm(time.strptime(image, fmt1))
		while (time0 > time2):
			temp = next(reader)
			temptime = calendar.timegm(time.strptime(temp[3], fmt2))
			data1 = data2
			time1 = time2
			data2 = temp
			time2 = temptime

		# choose the time that is closer
		if (time1 - time0) < (time2 - time0):
			writer.writerow([image] + processdata(data1))
		else:
			writer.writerow([image] + processdata(data2))
