var SunCalc = require('suncalc');

const readline = require('readline');
const fs = require('fs');

const rl = readline.createInterface({
	input: fs.createReadStream(process.argv[2])
});

inRange = function(date, loDate, hiDate) {
    if (date.getTime() > loDate.getTime() && date.getTime() < hiDate.getTime()) return 1;
    return 0;
}

// input this yourself
var lat = process.argv[3],
    lng = process.argv[4],
    offset = process.argv[5];

function convertDate(date) {
    var newDate = new Date(date.getTime()+offset*60*60*1000);
    return newDate;
}

s = ',' + 'sunrise-sunriseEnd' + ',' + 'sunriseEnd-goldenHourEnd' + ',' + 'goldenHourEnd-solarNoon'
+ ',' + 'solarNoon-goldenHour' + ',' + 'goldenHour-sunsetStart' + ',' + 'sunsetStart-sunset'
+ ',' + 'sunset-dusk' + ',' + 'dusk-nauticalDusk' + ',' + 'nauticalDusk-night'
+ ',' + 'night-nadir' + ',' + 'nadir-nightEnd' + ',' + 'nightEnd-nauticalDawn'
+ ',' + 'nauticalDawn-dawn' + ',' + 'sunrise-goldenHourEnd' + ',' + 'sunrise-sunset'
+ ',' + 'sunriseEnd-sunsetStart' + ',' + 'goldenHourEnd-goldenHour' + ',' + 'goldenHour-sunset'
+ ',' + 'sunset-night' + ',' + 'night-nightEnd';
console.log(s);

var defaultDate = new Date();
var sunTimes;
rl.on('line', (file) => { // read string

	// parse date
	var s = file.substring(0,4) + '-' + file.substring(4,6) + '-' + file.substring(6,8)
	+ ' ' + file.substring(9,11) + ':' + file.substring(11,13) + ':' + file.substring(13,15);
	var currentDate = new Date(s);
	var currentDate = convertDate(currentDate)

	// get sun calculations
	if (currentDate.getDate() != defaultDate.getDate() ||
	    currentDate.getMonth() != defaultDate.getMonth()) { // slight optimization
		defaultDate = currentDate;
		sunTimes = SunCalc.getTimes(defaultDate, lat, lng);
	}

	var test1  = inRange(currentDate, sunTimes.sunrise, sunTimes.sunriseEnd);
	var test2  = inRange(currentDate, sunTimes.sunriseEnd, sunTimes.goldenHourEnd);
	var test3  = inRange(currentDate, sunTimes.goldenHourEnd, sunTimes.solarNoon);
	var test4  = inRange(currentDate, sunTimes.solarNoon, sunTimes.goldenHour);
	var test5  = inRange(currentDate, sunTimes.goldenHour, sunTimes.sunsetStart);
	var test6  = inRange(currentDate, sunTimes.sunsetStart, sunTimes.sunset);
	var test7  = inRange(currentDate, sunTimes.sunset, sunTimes.dusk);
	var test8  = inRange(currentDate, sunTimes.dusk, sunTimes.nauticalDusk);
	var test9  = inRange(currentDate, sunTimes.nauticalDusk, sunTimes.night);
	var test10 = inRange(currentDate, sunTimes.night, sunTimes.nadir);
	var test11 = inRange(currentDate, sunTimes.nadir, sunTimes.nightEnd);
	var test12 = inRange(currentDate, sunTimes.nightEnd, sunTimes.nauticalDawn);
	var test13 = inRange(currentDate, sunTimes.nauticalDawn, sunTimes.dawn);

	var test14 = inRange(currentDate, sunTimes.sunrise, sunTimes.goldenHourEnd);
	var test15 = inRange(currentDate, sunTimes.sunrise, sunTimes.sunset);
	var test16 = inRange(currentDate, sunTimes.sunriseEnd, sunTimes.sunsetStart);
	var test17 = inRange(currentDate, sunTimes.goldenHourEnd, sunTimes.goldenHour);
	var test18 = inRange(currentDate, sunTimes.goldenHour, sunTimes.sunset);
	var test19 = inRange(currentDate, sunTimes.sunset, sunTimes.night);
	var test20 = inRange(currentDate, sunTimes.night, sunTimes.nightEnd);

	// print sun calculations
	s = file + ',' + test1 + ',' + test2 + ',' + test3 + ',' + test4 + ',' + test5+ ',' + test6
	+ ',' + test7 + ',' + test8 + ',' + test9 + ',' + test10 + ',' + test11 + ',' + test12
	+ ',' + test13 + ',' + test14 + ',' + test15 + ',' + test16 + ',' + test17 + ',' + test18
	+ ',' + test19 + ',' + test20;
	console.log(s);
});

